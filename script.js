document.querySelector('#channel-button').addEventListener('click', e => {
    if (document.querySelector('#name-of-channel').value.length > 0) {
        fetch('http://localhost:5000/channels/', {
         method: "POST",
         headers: new Headers({
             'Content-Type': 'application/json'
         }),
         body: JSON.stringify({
             name: document.querySelector('#name-of-channel').value
         })
     })
    .then(result => result.json())
    .then(data => {
        fetch_channels();
        console.log(data)
        
    })
    document.querySelector('#name-of-channel').value = "";
    }
    
    //prevents reloading/refreshing page
    e.preventDefault();
});
//channelId = null;
// fetching channels to display
async function fetch_channels(){
    let myNode = document.querySelector(".channels-list");
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }
    const channel_response = await fetch('http://localhost:5000/channels');
    const response_data = await channel_response.json();
    let channels_data = response_data.resources;
    for (let channel of channels_data){
        let channelId = channel.id;
        let div_creator = document.createElement("div");
        div_creator.id = channelId;
        div_creator.className = "channel-name-select"
        div_creator.onclick = function() { fetch_messages(this.id) };
        div_creator.appendChild(document.createTextNode(channel.name));
        document.querySelector(".channels-list").appendChild(div_creator);
        
    }
}
//calling fetchChannels to display All channels
fetch_channels()

//Display Messages
channelID = 1;
async function fetch_messages(channel_id){
    channelID = channel_id;
    const address = `http://localhost:5000/messages/?channel_id=${channel_id}`;
    const channel_response = await fetch(address);
    const msg_response = await channel_response.json();
    let messages_data = msg_response.resources;
    let myNode = document.querySelector(".messages-display");
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }
    for (var ele of document.getElementById(`${channel_id}`).parentElement.children){
        if (channel_id === ele.id){
            document.getElementById(`${ele.id}`).style.background = '#f0ffff';
            document.getElementById(`${ele.id}`).style.borderRadius = '50px';
            document.getElementById(`${ele.id}`).style.fontSize = "1.2rem"
        }
        else {
            document.getElementById(`${ele.id}`).style.background = '';
            document.getElementById(`${ele.id}`).style.borderRadius = '50px';
            document.getElementById(`${ele.id}`).style.fontSize = "1rem"
        }
    }
    for (let message of messages_data){
        let user_name = message.username;
        let message_text = message.text;
        
        //user div create
        let user_div = document.createElement("div");
        let user_class_name = "user-class-css";
        user_div.className = user_class_name;
        user_div.appendChild(document.createTextNode(user_name));
        document.querySelector('.messages-display').appendChild(user_div);

        //message div create
        let div_creator = document.createElement("div");
        let class_name = "message_inside";
        div_creator.className = class_name;
        div_creator.appendChild(document.createTextNode(message_text));
        document.querySelector('.messages-display').appendChild(div_creator); 
        
    }
}

//Send messages to DB
document.querySelector(".send-button").addEventListener('click', e => {
    
    if (document.querySelector('#user-name-input').value.length > 0){
        if (document.querySelector('#message-insert').value.length > 0) {           
            fetch('http://localhost:5000/messages/', {
                method: "POST",
                headers: new Headers({
                    'Content-Type': 'application/json'
                }),
                body: JSON.stringify({
                    text: document.querySelector('#message-insert').value,
                    channel_id: channelID,
                    username: document.querySelector('#user-name-input').value
                })
            })
            .then(result => result.json())
            .then(data => {
                fetch('http://localhost:5000/broadcast', {
                    method: "POST",
                    headers: new Headers({
                        'Content-Type': 'application/json'
                    }),
                    body: JSON.stringify(data)
                })
            })
            .catch(err => console.log(err));
            
            document.querySelector('#message-insert').value = "";
            document.querySelector('#user-name-input').value = "";
            
        }
    }
    e.preventDefault();
});


//pusher part
// Enable pusher logging - don't include this in production
var pusher = new Pusher('04bf97a2542fdfcf7d09', {
    cluster: 'ap2',
    forceTLS: true
  });

var channel = pusher.subscribe('messages');
    channel.bind('message-added', function(data) {
    console.log("data: ",JSON.stringify(data))
    document.getElementById(data.channel_id).click();
});


